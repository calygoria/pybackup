#!/usr/bin/env python
# -*- coding: utf-8 -*-

import distutils.log
import distutils.dir_util
import json
import argparse
import time
import os
import sys
from colorama import Fore, Back, Style
import humanize
from progress.bar import IncrementalBar
from progress.spinner import Spinner

############ Arguments ############
parser = argparse.ArgumentParser()
optional = parser._action_groups.pop()
required = parser.add_argument_group('required arguments')
required.add_argument("-c", "--config-file", help="path to the config file.")
parser._action_groups.append(optional)
args = parser.parse_args()

############ Welcome ############
print(Fore.BLUE + Style.BRIGHT + '''

    ██████╗░██╗░░░██╗██████╗░░█████╗░░█████╗░██╗░░██╗██╗░░░██╗██████╗░
    ██╔══██╗╚██╗░██╔╝██╔══██╗██╔══██╗██╔══██╗██║░██╔╝██║░░░██║██╔══██╗
    ██████╔╝░╚████╔╝░██████╦╝███████║██║░░╚═╝█████═╝░██║░░░██║██████╔╝
    ██╔═══╝░░░╚██╔╝░░██╔══██╗██╔══██║██║░░██╗██╔═██╗░██║░░░██║██╔═══╝░
    ██║░░░░░░░░██║░░░██████╦╝██║░░██║╚█████╔╝██║░╚██╗╚██████╔╝██║░░░░░
    ╚═╝░░░░░░░░╚═╝░░░╚═════╝░╚═╝░░╚═╝░╚════╝░╚═╝░░╚═╝░╚═════╝░╚═╝░░░░░
''' + Style.RESET_ALL)
print(Fore.BLUE + Style.BRIGHT + '''
                            𝚃𝚒𝚖𝚎 𝚝𝚘 𝚋𝚊𝚌𝚔𝚞𝚙

''' + Style.RESET_ALL)

############ Check required args ############
if len(sys.argv)==1:
    parser.print_help(sys.stderr)
    print("\n")
    sys.exit(1)

############ Load config ############
try:
    with open(args.config_file) as json_config_file:
        config = json.load(json_config_file)
except:
    print(Fore.RED + 'ERROR' + Style.RESET_ALL + " : Could not load config file \"" + Fore.YELLOW + args.config_file + Style.RESET_ALL + "\"\n")
    sys.exit(1)

############ Validate parameters ############
print("Backup destination :")
if os.path.isdir(config["backup_destination"]):
    print("[" + Fore.GREEN + "✓" + Style.RESET_ALL + "] \"" + Fore.YELLOW + config["backup_destination"] + Style.RESET_ALL + "\"" )
else:
    print("[" + Fore.RED + "×" + Style.RESET_ALL + "] \"" + Fore.YELLOW + config["backup_destination"] + Style.RESET_ALL + "\"" )
    print("\n" + Fore.RED + 'ERROR' + Style.RESET_ALL + " : \"" + Fore.YELLOW + config["backup_destination"] + Style.RESET_ALL + "\" is not a directory or does not exist.\n")
    sys.exit(1)

print("\nBackup sources :")
flag = False
backup_sources = []
for bs in config["backup_sources"] :
    if os.path.isdir(bs):
        print("[" + Fore.GREEN + "✓" + Style.RESET_ALL + "] \"" + Fore.YELLOW + bs + Style.RESET_ALL + "\"" )
        backup_sources.append(bs)
    else:
        flag = True
        print("[" + Fore.RED + "×" + Style.RESET_ALL + "] \"" + Fore.YELLOW + bs + Style.RESET_ALL + "\"" )

print("")
if flag:
    print(Fore.CYAN + 'WARNING' + Style.RESET_ALL + " : Some backup sources are missing, they will be ignored.")

while True:
    value = input("Would you like to proceed ? (y/n) ").lower()
    if value in ["y", "yes"]:
        break
    elif value in ["n", "no"]:
        print("Exiting.")
        sys.exit(0)
    else:
        pass

############ nbFiles and size ############
print("")
nbFiles = 0
total_size = 0
with IncrementalBar('Calculating', max=len(backup_sources)) as bar:
    for bs in backup_sources:
        for base, dirs, files in os.walk(bs):
            for f in files:
                nbFiles += 1
                fp = os.path.join(base, f)
                if not os.path.islink(fp):
                    total_size += os.path.getsize(fp)
            
        bar.next()

print("Found " + Fore.YELLOW + str(nbFiles) + Style.RESET_ALL + " file(s) to backup. Total size : " + Fore.YELLOW + humanize.naturalsize(total_size) + Style.RESET_ALL + ".\n")

############ Copy tree ############
start = time.time()
with IncrementalBar('Copying', max=len(backup_sources)) as bar:
    for bs in backup_sources:
        distutils.dir_util.copy_tree(
            bs,
            os.path.join(config["backup_destination"], os.path.basename(bs)),
            update=1,
        )
        bar.next()

############ End ############
duration = time.time() - start
print("Backup done in " + Fore.YELLOW + str(int(duration)) + Style.RESET_ALL + " second(s).\n")
