# PyBackup

This script is made to backup folder trees to an external hard drive. It copies the files *only* if they are new or have been modified since the last backup. 

The script is configured by a JSON file wich is passed as a parameter :
- the `backup_sources` are the folders to be copied
- the `backup_destination` is the place where they will be copied

This allows the following workflow :

![PyBackup](PyBackup.png)

## Prequisites

```bash
python3 -m pip install colorama
python3 -m pip install humanize
python3 -m pip install progress
```

## Usage

```bash
usage: pybackup.py [-h] [-c CONFIG_FILE]

required arguments:
  -c CONFIG_FILE, --config-file CONFIG_FILE
                        path to the config file.

optional arguments:
  -h, --help            show this help message and exit
```

## Example

Plug external hard drive and mount it to `E:\`. Then launch the script :

```bash
python3 .\pybackup.py -c E:\config.json
```

with `E:\config.json` (here the config is stored on the HDD but can be anywhere) :

```json
{
    "backup_destination": "E:\\calan_backup\\",
    "backup_sources": [
        "D:\\Photos",
        "D:\\Documents",
        "D:\\Films"
    ]
}
```